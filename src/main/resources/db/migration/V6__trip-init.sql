CREATE TABLE trip
(
    id BIGSERIAL PRIMARY KEY,
    vehicle_id BIGSERIAL,
    site_id BIGSERIAL,
    driver_id BIGSERIAL,
    date_time TIMESTAMP,
    available_seats INTEGER,
    pending_confirmed_seats INTEGER,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (site_id) REFERENCES site(id) ON DELETE CASCADE ON UPDATE CASCADE
);