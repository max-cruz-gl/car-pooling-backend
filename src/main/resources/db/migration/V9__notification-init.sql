CREATE TABLE notification
(
    id BIGSERIAL PRIMARY KEY,
    trip_id BIGSERIAL,
    vehicle_id BIGSERIAL,
    user_account_id BIGSERIAL,
    type CHARACTER VARYING(255),
    transmitter BIGSERIAL,
    date_time TIMESTAMP,
    status CHARACTER VARYING(255),
    description CHARACTER VARYING(255),
    FOREIGN KEY (trip_id) REFERENCES trip(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_account_id) REFERENCES user_account(id) ON DELETE CASCADE ON UPDATE CASCADE
);