CREATE TABLE passenger
(
    user_account_id BIGSERIAL,
    trip_id BIGSERIAL,
    FOREIGN KEY (user_account_id) REFERENCES user_account(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (trip_id) REFERENCES trip(id) ON DELETE CASCADE ON UPDATE CASCADE
);