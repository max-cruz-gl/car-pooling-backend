CREATE TABLE coordinate
(
    id BIGSERIAL PRIMARY KEY,
    trip_id BIGSERIAL,
    vehicle_id BIGSERIAL,
    latitude DECIMAL,
    longitude DECIMAL,
    position INTEGER,
    FOREIGN KEY (trip_id) REFERENCES trip(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(id) ON DELETE CASCADE ON UPDATE CASCADE
);