package com.globantlabs.config.security;

import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class CarPoolingUserDetailsService implements UserDetailsService {


  @Autowired
  protected UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) {

    UserAccount user = userRepository.findByEmail(username);

    if (user == null) {
      throw new UserInvalidException("");
    }

    OAuthClient oauthClient = new OAuthClient();
    oauthClient.setEmail(user.getEmail());
    oauthClient.setGoogleToken(user.getGoogleToken());

    UserDetails details = new ClientRepositoryUserDetails(oauthClient);

    if (!details.isEnabled()) {
      throw new UserInvalidException("");
    }

    return details;
  }

}