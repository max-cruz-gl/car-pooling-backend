package com.globantlabs.config.security;

import java.io.Serializable;

public class OAuthClient implements Serializable {

  private static final long serialVersionUID = 1L;

  protected String email;
  protected String googleToken;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getGoogleToken() {
    return googleToken;
  }

  public void setGoogleToken(String googleToken) {
    this.googleToken = googleToken;
  }
}
