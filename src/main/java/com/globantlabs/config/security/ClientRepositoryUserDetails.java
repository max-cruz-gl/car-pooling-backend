package com.globantlabs.config.security;

import com.globantlabs.carpooling.util.statics.OAuthStatics;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ClientRepositoryUserDetails implements UserDetails, Serializable {

  private static final long serialVersionUID = 1L;

  protected OAuthClient user;

  /**
   * .
   */
  public ClientRepositoryUserDetails(OAuthClient client) {
    super();
    
    this.user = client;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    GrantedAuthority roleUser = new GrantedAuthority() {

      @Override
      public String getAuthority() {
        return OAuthStatics.ROLE_USER;
      }
    };

    List<GrantedAuthority> roles = new LinkedList<>();
    roles.add(roleUser);

    return roles;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public String getUsername() {
    return user.getEmail();
  }

  @Override
  public String getPassword() {
    return user.getGoogleToken();
  }
}
