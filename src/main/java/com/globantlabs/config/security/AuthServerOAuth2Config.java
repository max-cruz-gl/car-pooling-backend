package com.globantlabs.config.security;

import com.globantlabs.carpooling.util.statics.OAuthStatics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
public class AuthServerOAuth2Config extends AuthorizationServerConfigurerAdapter {

  @Autowired
  protected CarPoolingUserDetailsService carPoolingUserDetailsService;

  @Autowired
  protected DataSource dataSource;

  @Autowired
  private TokenStore tokenStore;

  @Autowired
  @Qualifier("authenticationManagerBean")
  private AuthenticationManager authenticationManager;

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

    clients
      .inMemory()
        .withClient(OAuthStatics.CP_CLIENT)
        .secret(OAuthStatics.CP_SECRET)
        .resourceIds(OAuthStatics.RESOURCE_ID)
        .scopes("openid")
        .autoApprove(true)
        .authorizedGrantTypes("password", "refresh_token")
        .accessTokenValiditySeconds(OAuthStatics.ACCESS_TOKEN_EXPIRATION)
        .refreshTokenValiditySeconds(OAuthStatics.REFRESH_TOKEN_EXPIRATION);

  }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints
      .tokenStore(tokenStore)
      .userDetailsService(carPoolingUserDetailsService)
      .authenticationManager(authenticationManager);
  }

}