package com.globantlabs.config.security;

import com.globantlabs.carpooling.util.statics.OAuthStatics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired
  private TokenStore tokenStore;

  @Override
  public void configure(ResourceServerSecurityConfigurer resources)
      throws Exception {
    resources
        .tokenStore(tokenStore)
        .resourceId(OAuthStatics.RESOURCE_ID);
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http
    .authorizeRequests()
      .antMatchers("/user/login").permitAll()
      .antMatchers("/site/**").permitAll()
      .antMatchers("/user/profile").authenticated()
      .antMatchers("/user").authenticated()
    .and()
      .exceptionHandling()
        .accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
  }

}