package com.globantlabs.carpooling.model;

public class GenericNotification {

  private String type;
  private long transmitter;
  private String dateTime;
  private String status;
  private String description;
  private long tripId;
  private long vehicleId;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public long getTransmitter() {
    return transmitter;
  }

  public void setTransmitter(long transmitter) {
    this.transmitter = transmitter;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public long getTripId() {
    return tripId;
  }

  public void setTripId(long tripId) {
    this.tripId = tripId;
  }

  public long getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(long vehicleId) {
    this.vehicleId = vehicleId;
  }
}
