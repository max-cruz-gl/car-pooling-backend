package com.globantlabs.carpooling.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
public class Login {

  protected long id;

  @Id
  protected String auth;
  protected String access;
  protected String refresh;
  protected Date start;
  protected Date finish;

  public static final int SESSION_EXPIRATION = 10;

  public Login() {
    auth = UUID.randomUUID().toString();

    start = new Date();

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(start);
    calendar.add(Calendar.HOUR, SESSION_EXPIRATION);

    finish = calendar.getTime();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getAuth() {
    return auth;
  }

  public void setAuth(String auth) {
    this.auth = auth;
  }

  public String getAccess() {
    return access;
  }

  public void setAccess(String access) {
    this.access = access;
  }

  public String getRefresh() {
    return refresh;
  }

  public void setRefresh(String refresh) {
    this.refresh = refresh;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getFinish() {
    return finish;
  }

  public void setFinish(Date finish) {
    this.finish = finish;
  }

}


