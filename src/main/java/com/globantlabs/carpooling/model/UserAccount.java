package com.globantlabs.carpooling.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class UserAccount {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private long id;
  private String name;
  private String lastName;
  private String address;
  private String email;
  private String pictureUrl;
  private String googleToken;
  private boolean driver;
  private Date lastAction;
  private String status;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "site_id")
  private Site site;

  @OneToMany(mappedBy = "userAccount")
  private List<Vehicle> vehicles;

  @ManyToMany(mappedBy = "userAccounts")
  private List<Trip> trips;

  @OneToMany(mappedBy = "userAccount")
  private List<Notification> notifications;

  public UserAccount() {
    this.lastAction = new Date();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPictureUrl() {
    return pictureUrl;
  }

  public void setPictureUrl(String pictureUrl) {
    this.pictureUrl = pictureUrl;
  }

  public String getGoogleToken() {
    return googleToken;
  }

  public void setGoogleToken(String googleToken) {
    this.googleToken = googleToken;
  }

  public Date getLastAction() {
    return lastAction;
  }

  public void setLastAction(Date lastAction) {
    this.lastAction = lastAction;
  }

  public boolean isDriver() {
    return driver;
  }

  public void setDriver(boolean driver) {
    this.driver = driver;
  }

  public Site getSite() {
    return site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  public void updateLastAction() {
    this.lastAction = new Date();
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<Vehicle> getVehicles() {
    return vehicles;
  }

  public void setVehicles(List<Vehicle> vehicles) {
    this.vehicles = vehicles;
  }

  public List<Trip> getTrips() {
    return trips;
  }

  public void setTrips(List<Trip> trips) {
    this.trips = trips;
  }

  public List<Notification> getNotifications() {
    return notifications;
  }

  public void setNotifications(List<Notification> notifications) {
    this.notifications = notifications;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserAccount that = (UserAccount) o;

    if (id != that.id) {
      return false;
    }
    if (!name.equals(that.name)) {
      return false;
    }
    if (!lastName.equals(that.lastName)) {
      return false;
    }
    if (address != null ? !address.equals(that.address) : that.address != null) {
      return false;
    }
    if (!email.equals(that.email)) {
      return false;
    }
    if (!pictureUrl.equals(that.pictureUrl)) {
      return false;
    }
    return googleToken.equals(that.googleToken);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + name.hashCode();
    result = 31 * result + lastName.hashCode();
    result = 31 * result + (address != null ? address.hashCode() : 0);
    result = 31 * result + email.hashCode();
    result = 31 * result + pictureUrl.hashCode();
    result = 31 * result + googleToken.hashCode();
    return result;
  }
}
