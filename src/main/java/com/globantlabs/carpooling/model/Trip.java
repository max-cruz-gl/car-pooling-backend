package com.globantlabs.carpooling.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.h2.engine.User;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class Trip {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  private Date dateTime;
  private int availableSeats;
  private int pendingConfirmedSeats;
  private long driverId;

  @ManyToOne
  @JoinColumn(name = "vehicle_id")
  private Vehicle vehicle;

  @ManyToOne
  @JoinColumn(name = "site_id")
  private Site site;

  @OneToMany(mappedBy = "trip")
  private List<Coordinate> coordinates;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "passenger",
      joinColumns = @JoinColumn(name = "trip_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"))
  private List<UserAccount> userAccounts;

  @OneToMany(mappedBy = "trip")
  private List<Notification> notifications;

  public Trip() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public void setDateTime(Date dateTime) {
    this.dateTime = dateTime;
  }

  public int getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(int availableSeats) {
    this.availableSeats = availableSeats;
  }

  public int getPendingConfirmedSeats() {
    return pendingConfirmedSeats;
  }

  public void setPendingConfirmedSeats(int pendingConfirmedSeats) {
    this.pendingConfirmedSeats = pendingConfirmedSeats;
  }

  public long getDriverId() {
    return driverId;
  }

  public void setDriverId(long driverId) {
    this.driverId = driverId;
  }

  @JsonIgnore
  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  @JsonIgnore
  public Site getSite() {
    return site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  @JsonIgnore
  public List<Coordinate> getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(List<Coordinate> coordinates) {
    this.coordinates = coordinates;
  }

  @JsonIgnore
  public List<UserAccount> getUserAccounts() {
    return userAccounts;
  }

  public void setUserAccounts(List<UserAccount> userAccounts) {
    this.userAccounts = userAccounts;
  }

  @JsonIgnore
  public List<Notification> getNotifications() {
    return notifications;
  }

  public void setNotifications(List<Notification> notifications) {
    this.notifications = notifications;
  }

  public void addPendingTrip() {
    this.pendingConfirmedSeats++;
  }

  public void confirmTrip(UserAccount passenger) {
    this.pendingConfirmedSeats--;
    this.availableSeats--;
    userAccounts.add(passenger);
  }

  public void reject() {
    this.pendingConfirmedSeats--;
  }
}
