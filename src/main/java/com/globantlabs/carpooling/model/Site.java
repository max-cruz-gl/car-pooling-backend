package com.globantlabs.carpooling.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import java.util.List;

@Entity
public class Site {

  @Id
  @TableGenerator(name = "tab", initialValue = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "tab")
  private long id;
  private String name;
  private String code;
  private String country;
  private String city;
  private String address;
  private double latitude;
  private double longitude;

  @OneToMany(mappedBy = "site")
  private List<UserAccount> userAccount;

  @OneToMany(mappedBy = "site")
  private List<Trip> trips;

  public Site() {
  }

  public Site(long id) {
    this.id = id;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  @JsonIgnore
  public List<UserAccount> getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(List<UserAccount> userAccount) {
    this.userAccount = userAccount;
  }

  @JsonIgnore
  public List<Trip> getTrips() {
    return trips;
  }

  public void setTrips(List<Trip> trips) {
    this.trips = trips;
  }
}
