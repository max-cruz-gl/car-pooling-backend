package com.globantlabs.carpooling.service.notification.firebase;

import com.globantlabs.carpooling.facade.FirebasePushNotificationFacade;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FirebasePushNotificationServiceImpl implements FirebasePushNotificationService {

  @Autowired
  private FirebasePushNotificationFacade firebaseFacade;

  @Override
  public void sendDriverMessageRequest(UserAccount passenger, UserAccount driver, Trip trip) {
    //todo: firebase connection
  }

  @Override
  public void sendPassengeMessageAcepted(UserAccount passenger, Trip trip) {
    //todo: firebase connection
  }

  @Override
  public void sendPassengeMessageRejected(UserAccount passenger, Trip trip) {
    //todo: firebase connection
  }

  @Override
  public void sendPassengeMessageDeleted(UserAccount userAccount) {
    //todo: firebase connection
  }
}
