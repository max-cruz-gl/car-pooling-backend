package com.globantlabs.carpooling.service.notification.firebase;

import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;

public interface FirebasePushNotificationService {

  void sendDriverMessageRequest(UserAccount passenger,UserAccount driver, Trip trip);

  void sendPassengeMessageAcepted(UserAccount passenger, Trip trip);

  void sendPassengeMessageRejected(UserAccount passenger, Trip trip);

  void sendPassengeMessageDeleted(UserAccount userAccount);
}
