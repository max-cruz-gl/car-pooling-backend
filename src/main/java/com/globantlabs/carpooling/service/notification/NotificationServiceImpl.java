package com.globantlabs.carpooling.service.notification;

import com.globantlabs.carpooling.controller.responses.notification.NotificationsResponse;
import com.globantlabs.carpooling.model.GenericNotification;
import com.globantlabs.carpooling.model.Notification;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.repository.NotificationRepository;
import com.globantlabs.carpooling.repository.UserRepository;
import com.globantlabs.carpooling.service.notification.firebase.FirebasePushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.globantlabs.carpooling.util.statics.NotificationStatics.DRIVER_RESPONSE_TRIP;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.PASSENGER_ACCEPTED_DESCRIPTION;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.PASSENGER_REJECTED_DESCRIPTION;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.PASSENGER_REQUEST_TRIP;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.PASSENGER_REQUEST_TRIP_DESCRIPTION;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.STATUS_ACCEPTED;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.DRIVER_ACCEPTED_DESCRIPTION;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.STATUS_PENDING_APPROVAL;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.STATUS_REJECTED;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.DRIVER_REJECTED_DESCRIPTION;

@Service
public class NotificationServiceImpl implements NotificationService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private NotificationRepository notificationRepository;

  @Autowired
  private FirebasePushNotificationService firebaseService;

  @Override
  public void pushDriver(UserAccount userAccount, Trip trip) {

    UserAccount driver = trip.getVehicle().getUserAccount();

    firebaseService.sendDriverMessageRequest(userAccount, driver, trip);

    createNotification(trip, userAccount, driver, STATUS_PENDING_APPROVAL,
        PASSENGER_REQUEST_TRIP, PASSENGER_REQUEST_TRIP_DESCRIPTION);
  }

  @Override
  public void pushPassengetAccepted(Trip trip, UserAccount passenger) {

    firebaseService.sendPassengeMessageAcepted(passenger, trip);

    updateNotificationFrom(trip, passenger, STATUS_ACCEPTED, DRIVER_ACCEPTED_DESCRIPTION);

    UserAccount driver = trip.getVehicle().getUserAccount();
    createNotification(trip, driver, passenger, STATUS_ACCEPTED, DRIVER_RESPONSE_TRIP,
        PASSENGER_ACCEPTED_DESCRIPTION);
  }

  @Override
  public void pushPassengetRejected(Trip trip, UserAccount passenger, String description) {
    Notification notification = notificationRepository.findByTripAndTransmitter(trip, passenger.getId());
    notification.setStatus(STATUS_REJECTED);
    notification.setType(DRIVER_RESPONSE_TRIP);
    notification.setDescription(DRIVER_REJECTED_DESCRIPTION + description);
    notificationRepository.save(notification);

    UserAccount driver = trip.getVehicle().getUserAccount();
    createNotification(trip, driver, passenger, STATUS_REJECTED, DRIVER_RESPONSE_TRIP,
        PASSENGER_REJECTED_DESCRIPTION);
    firebaseService.sendPassengeMessageRejected(passenger, trip);
  }

  @Override
  public void sendDeletedNotification(List<UserAccount> userAccounts) {
    for(UserAccount userAccount : userAccounts) {
      firebaseService.sendPassengeMessageDeleted(userAccount);
    }
  }

  @Override
  public NotificationsResponse getReceivedNotifications(UserAccount userAccount) {
    return parseNotifications(userAccount.getNotifications());
  }

  @Override
  public NotificationsResponse getSendNotifications(UserAccount userAccount) {
    List<Notification> notifications = notificationRepository.findByTransmitter(userAccount.getId());
    return parseNotifications(notifications);
  }

  private void createNotification(Trip trip, UserAccount from, UserAccount to, String status,
                                  String type, String description) {
    Notification notification = new Notification();

    notification.setStatus(status);
    notification.setType(type);
    notification.setTransmitter(from.getId());
    notification.setDescription(description);
    notification.setTrip(trip);
    notification.setVehicle(trip.getVehicle());
    notification.setDateTime(new Date());
    notification.setUserAccount(to);

    notificationRepository.save(notification);
  }

  private void updateNotificationFrom(Trip trip, UserAccount transmitter, String status, String description) {
    Notification notification = notificationRepository.findByTripAndTransmitter(trip, transmitter.getId());

    notification.setStatus(status);
    notification.setDescription(description);

    notificationRepository.save(notification);
  }

  private NotificationsResponse parseNotifications(List<Notification> notifications) {
    NotificationsResponse response = new NotificationsResponse();
    List<GenericNotification> genericNotifications = new ArrayList<>();

    for (Notification notification : notifications) {
      GenericNotification genericNotification = new GenericNotification();
      genericNotification.setType(notification.getType());
      genericNotification.setTransmitter(notification.getTransmitter());
      genericNotification.setDateTime(notification.getDateTime().toString());
      genericNotification.setStatus(notification.getStatus());
      genericNotification.setDescription(notification.getDescription());
      genericNotification.setTripId(notification.getTrip().getId());
      genericNotification.setVehicleId(notification.getVehicle().getId());
      genericNotifications.add(genericNotification);
    }

    response.setNotifications(genericNotifications);
    return response;
  }
}
