package com.globantlabs.carpooling.service.notification;

import com.globantlabs.carpooling.controller.responses.notification.NotificationsResponse;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;

import java.util.List;

public interface NotificationService {

  void pushDriver(UserAccount userAccount, Trip trip);

  void pushPassengetAccepted(Trip trip, UserAccount passenger);

  void pushPassengetRejected(Trip trip, UserAccount passenger, String description);

  void sendDeletedNotification(List<UserAccount> userAccounts);

  NotificationsResponse getReceivedNotifications(UserAccount userAccount);

  NotificationsResponse getSendNotifications(UserAccount userAccount);
}
