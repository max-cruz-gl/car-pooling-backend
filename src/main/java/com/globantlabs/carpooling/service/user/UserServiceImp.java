package com.globantlabs.carpooling.service.user;

import com.globantlabs.carpooling.controller.request.user.UserLoginRequest;
import com.globantlabs.carpooling.controller.request.user.UserUpdateInformationRequest;
import com.globantlabs.carpooling.controller.responses.user.UserAccountResponse;
import com.globantlabs.carpooling.controller.responses.user.UserLoginResponse;
import com.globantlabs.carpooling.exception.google.GoogleUserInformationApiException;
import com.globantlabs.carpooling.facade.GoogleUserInformationApiFacade;
import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.repository.UserRepository;
import com.globantlabs.carpooling.service.auth.AuthService;
import com.globantlabs.carpooling.service.site.SiteService;
import com.globantlabs.carpooling.service.user.status.UserStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;

import static com.globantlabs.carpooling.util.statics.UserStatusStatics.STATUS_ADDITIONAL_COMPLETED;
import static com.globantlabs.carpooling.util.statics.UserStatusStatics.STATUS_COMPLETED;
import static com.globantlabs.carpooling.util.statics.UserStatusStatics.STATUS_REGISTER_COMPLETED;

@Service
public class UserServiceImp implements UserService {

  private final static long INITIAL_SITE = 99L;

  @Autowired
  private AuthService authService;

  @Autowired
  private UserStatusService userStatusService;

  @Autowired
  private SiteService siteService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  GoogleUserInformationApiFacade googleApiFacade;

  @Override
  public UserAccountResponse getUserInformation(UserAccount userAccount) {
    return parseUserAccountToResponse(userAccount);
  }

  @Override
  public UserLoginResponse loginUser(UserLoginRequest userLoginRequest) {

    UserAccount userAccount = userRepository.findByEmail(userLoginRequest.getEmail());

    if (userAccount == null) {
      userAccount = new UserAccount();
      userAccount.setEmail(userLoginRequest.getEmail());
      userAccount.setGoogleToken(userLoginRequest.getGoogleToken());
      try {
        userAccount = googleApiFacade.getUserInformation(userAccount);
        userAccount.setStatus(STATUS_REGISTER_COMPLETED);
        Site initialsite = siteService.getSite(INITIAL_SITE);
        userAccount.setSite(initialsite);
      } catch (IOException e) {
        throw new GoogleUserInformationApiException();
      }
    }

    userAccount.setGoogleToken(userLoginRequest.getGoogleToken());
    save(userAccount);

    OAuth2AccessToken oauth2AccessToken = authService.login(userAccount.getEmail(), userAccount.getGoogleToken());

    UserLoginResponse response = new UserLoginResponse();

    response.setAccessToken(oauth2AccessToken.getValue());
    response.setRefreshToken(oauth2AccessToken.getRefreshToken().getValue());

    return response;
  }

  @Override
  public void updateUserInformation(UserAccount userAccount, UserUpdateInformationRequest request) {

    if (!StringUtils.isEmpty(request.getAddress())) {
      userAccount.setAddress(request.getAddress());
    }

    if (!StringUtils.isEmpty(request.getSite())) {
      Site site = siteService.getSite(request.getSite());
      userAccount.setSite(site);
    }

    if (!StringUtils.isEmpty(userAccount.getAddress()) && userAccount.getSite() != null) {
      userAccount.setStatus(STATUS_ADDITIONAL_COMPLETED);
    }

    if (!StringUtils.isEmpty(request.getDriver())) {
      boolean isDriver = Boolean.parseBoolean(request.getDriver());
      userAccount.setDriver(isDriver);
      userAccount.setStatus(STATUS_COMPLETED);
    }
    save(userAccount);
  }

  public void save(UserAccount userAccount) {
    userAccount.updateLastAction();
    userRepository.save(userAccount);
  }

  private UserAccountResponse parseUserAccountToResponse(UserAccount userAccount) {
    UserAccountResponse response = new UserAccountResponse();
    response.setName(userAccount.getName());
    response.setLastName(userAccount.getLastName());
    response.setAddress(userAccount.getAddress());
    response.setDriver(userAccount.isDriver());
    response.setEmail(userAccount.getEmail());
    response.setPictureUrl(userAccount.getPictureUrl());
    response.setUserStatus(userAccount.getStatus());
    response.setLastAction(userAccount.getLastAction());
    response.setSite(userAccount.getSite().getId());
    return response;
  }
}
