package com.globantlabs.carpooling.service.user.status;

import com.globantlabs.carpooling.model.UserAccount;

public interface UserStatusService {
  void register(UserAccount userAccount);
}
