package com.globantlabs.carpooling.service.user;

import com.globantlabs.carpooling.controller.request.user.UserLoginRequest;
import com.globantlabs.carpooling.controller.request.user.UserUpdateInformationRequest;
import com.globantlabs.carpooling.controller.responses.user.UserAccountResponse;
import com.globantlabs.carpooling.controller.responses.user.UserLoginResponse;
import com.globantlabs.carpooling.model.UserAccount;

public interface UserService {

    UserAccountResponse getUserInformation(UserAccount userAccoun);

    void updateUserInformation(UserAccount userAccount,
                                      UserUpdateInformationRequest userUpdateInformationRequest);

    UserLoginResponse loginUser(UserLoginRequest userLoginRequest);

    void save(UserAccount userAccount);
}
