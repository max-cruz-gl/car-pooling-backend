package com.globantlabs.carpooling.service.user.credentials.login;

import com.globantlabs.carpooling.exception.carpooling.InvalidTokenException;
import com.globantlabs.carpooling.exception.carpooling.TokenExpiredException;
import com.globantlabs.carpooling.model.Login;
import com.globantlabs.carpooling.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LoginServiceImpl implements LoginService {

  @Autowired
  protected LoginRepository loginRepository;

  @Override
  public Login create(String access, String refresh) {
    Login session = new Login();
    session.setAccess(access);
    session.setRefresh(refresh);

    return loginRepository.save(session);
  }

  @Override
  public Login getByAuth(String auth) {
    Login login = loginRepository.findOne(auth);

    if (login == null) {
      throw new InvalidTokenException(auth);
    }

    if (isExpired(login)) {
      remove(auth);
      throw new TokenExpiredException(auth);
    }

    return login;
  }

  @Override
  public void remove(String auth) {
    loginRepository.delete(auth);
  }

  @Override
  public boolean isExpired(Login token) {
    return token.getFinish().before(new Date());
  }
}
