package com.globantlabs.carpooling.service.user.credentials.login;

import com.globantlabs.carpooling.model.Login;

public interface LoginService {

  public Login create(String access, String refresh);

  public Login getByAuth(String auth);

  public void remove(String auth);

  public boolean isExpired(Login token);
}
