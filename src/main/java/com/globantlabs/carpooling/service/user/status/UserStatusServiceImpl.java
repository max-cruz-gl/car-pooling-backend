package com.globantlabs.carpooling.service.user.status;

import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.globantlabs.carpooling.util.statics.UserStatusStatics.STATUS_REGISTER_COMPLETED;

@Service
public class UserStatusServiceImpl implements UserStatusService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public void register(UserAccount userAccount) {
    userAccount.setStatus(STATUS_REGISTER_COMPLETED);
    userRepository.save(userAccount);
  }
}
