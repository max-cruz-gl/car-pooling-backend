package com.globantlabs.carpooling.service.trip;

import com.globantlabs.carpooling.controller.request.trip.TripCreationRequest;
import com.globantlabs.carpooling.controller.request.trip.TripResponseRequest;
import com.globantlabs.carpooling.controller.request.trip.TripSearchRequest;
import com.globantlabs.carpooling.controller.request.trip.TripUpdateRequest;
import com.globantlabs.carpooling.controller.responses.trip.TripInformationResponse;
import com.globantlabs.carpooling.controller.responses.trip.TripSearchResponse;
import com.globantlabs.carpooling.model.UserAccount;

public interface TripService {

  void create(UserAccount userAccount, TripCreationRequest request);

  TripSearchResponse search(TripSearchRequest tripSearchRequest);

  TripInformationResponse getTrip(long id);

  void request(long id, UserAccount userAccount);

  void response(TripResponseRequest request);

  void deleteTrip(long id, UserAccount userAccount);

  void updateTrip(long id, UserAccount userAccount, TripUpdateRequest tripUpdateRequest);
}
