package com.globantlabs.carpooling.service.trip;

import com.globantlabs.carpooling.controller.request.trip.TripCreationRequest;
import com.globantlabs.carpooling.controller.request.trip.TripResponseRequest;
import com.globantlabs.carpooling.controller.request.trip.TripSearchRequest;
import com.globantlabs.carpooling.controller.request.trip.TripUpdateRequest;
import com.globantlabs.carpooling.controller.responses.trip.TripInformationResponse;
import com.globantlabs.carpooling.controller.responses.trip.TripSearchResponse;
import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.exception.carpooling.site.SiteNotFoundException;
import com.globantlabs.carpooling.exception.carpooling.trip.TripInvalidException;
import com.globantlabs.carpooling.exception.carpooling.trip.TripNotFoundException;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VechicleInvalidException;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VehicleNotFoundException;
import com.globantlabs.carpooling.model.Coordinate;
import com.globantlabs.carpooling.model.GenericPoint;
import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.model.Vehicle;
import com.globantlabs.carpooling.repository.CoordinateRepository;
import com.globantlabs.carpooling.repository.TripRepository;
import com.globantlabs.carpooling.repository.UserRepository;
import com.globantlabs.carpooling.repository.VehicleRepository;
import com.globantlabs.carpooling.service.coordinate.CoordinateService;
import com.globantlabs.carpooling.service.notification.NotificationService;
import com.globantlabs.carpooling.service.site.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.globantlabs.carpooling.util.date.DateUtils.parseStringToDate;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.STATUS_ACCEPTED;
import static com.globantlabs.carpooling.util.statics.NotificationStatics.STATUS_REJECTED;

@Service
public class TripServiceImpl implements TripService {

  @Autowired
  private TripRepository tripRepository;

  @Autowired
  private VehicleRepository vehicleRepository;

  @Autowired
  private CoordinateRepository coordinateRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SiteService siteService;

  @Autowired
  private CoordinateService coordinateService;

  @Autowired
  private NotificationService notificationService;

  @Override
  public void create(UserAccount userAccount, TripCreationRequest request) {

    if (userAccount.getVehicles().isEmpty()) {
      throw new VehicleNotFoundException();
    }

    if (!userAccount.isDriver()) {
      throw new UserInvalidException();
    }

    Trip trip = new Trip();

    Vehicle vehicle = vehicleRepository.findOne(request.getVehicleId());

    if (vehicle == null) {
      throw new VehicleNotFoundException();
    }

    if (vehicle.getUserAccount().getId() != userAccount.getId()) {
      throw new VechicleInvalidException();
    }

    trip.setVehicle(vehicle);

    Site site = siteService.getSite(request.getSiteId());

    if (site == null) {
      throw new SiteNotFoundException();
    }

    trip.setSite(site);

    List<UserAccount> userAccounts = new ArrayList<>();
    userAccounts.add(userAccount);

    trip.setUserAccounts(userAccounts);

    trip.setDateTime(parseStringToDate(request.getDateTime()));
    trip.setAvailableSeats(request.getAvailableSeats());
    trip.setDriverId(userAccount.getId());

    tripRepository.save(trip);
    saveCoordinates(request.getCoordinates(), trip);
  }

  @Override
  public TripSearchResponse search(TripSearchRequest request) {
    TripSearchResponse tripSearchResponse = new TripSearchResponse();
    Site site = siteService.getSite(request.getFinalSiteId());
    List<Trip> trips;

    if (request.getInitialPosition() == null && request.getMaximumWalkingDistance() == null) {
      trips = getAllTripsBetweenDates(request, site);
    } else {
      trips = getAllCloseTrips(request, site);
    }

    tripSearchResponse.setTrips(trips);
    return tripSearchResponse;
  }

  @Override
  public TripInformationResponse getTrip(long id) {
    Trip trip = tripRepository.findOne(id);
    if (trip == null) {
      throw new TripNotFoundException();
    }

    TripInformationResponse response = new TripInformationResponse();

    response.setAvailableSeats(trip.getAvailableSeats());
    response.setPendingConfirmedSeats(trip.getPendingConfirmedSeats());
    response.setDateTime(trip.getDateTime().toString());
    response.setVehicle(trip.getVehicle());
    response.setCoordinates(trip.getCoordinates());
    response.setDriverId(trip.getVehicle().getUserAccount().getId());

    return response;
  }

  @Override
  public void request(long id, UserAccount userAccount) {
    Trip trip = tripRepository.findOne(id);

    if (trip.getAvailableSeats() <= 0) {
      throw new TripInvalidException();
    }

    if (trip.getUserAccounts().contains(userAccount)) {
      throw new UserInvalidException();
    }

    trip.addPendingTrip();
    tripRepository.save(trip);
    notificationService.pushDriver(userAccount, trip);
  }

  @Override
  public void response(TripResponseRequest request) {
    Trip trip = tripRepository.findOne(request.getTripId());
    UserAccount passenger = userRepository.findOne(request.getPassengerId());
    if (STATUS_ACCEPTED.equals(request.getStatus())) {
      notificationService.pushPassengetAccepted(trip, passenger);
      trip.confirmTrip(passenger);

    } else if (STATUS_REJECTED.equals(request.getStatus())) {
      notificationService.pushPassengetRejected(trip, passenger, request.getDescription());
      trip.reject();
    }

    tripRepository.save(trip);
  }

  @Override
  public void deleteTrip(long id, UserAccount userAccount) {
    Trip trip = tripRepository.findOne(id);
    if (trip.getDriverId() != userAccount.getId()) {
      throw new UserInvalidException();
    }
    notificationService.sendDeletedNotification(trip.getUserAccounts());
    tripRepository.delete(trip);
  }

  @Override
  public void updateTrip(long id, UserAccount userAccount, TripUpdateRequest request) {
    Trip trip = tripRepository.findOne(id);
    if (trip == null) {
      throw new TripInvalidException();
    }

    if (trip.getDriverId() != userAccount.getId()) {
      throw new UserInvalidException();
    }

    if (request.getVehicleId() != null) {

      Vehicle vehicle = vehicleRepository.findOne(request.getVehicleId());

      if (vehicle == null) {
        throw new VechicleInvalidException();
      }

      if (userAccount.getId() != vehicle.getUserAccount().getId()) {
        throw new UserInvalidException();
      }
      trip.setVehicle(vehicle);
    }

    if (request.getAvailableSeats() != null) {
      trip.setAvailableSeats(request.getAvailableSeats());
    }

    if (request.getCoordinates() != null && trip.getCoordinates() != null) {
      deleteOldCoordinates(trip.getCoordinates());
      saveCoordinates(request.getCoordinates(), trip);
    }

    if (request.getSiteId() != null) {
      Site site = siteService.getSite(request.getSiteId());
      if (site == null) {
        throw new SiteNotFoundException();
      }
      trip.setSite(site);
    }

    if (request.getDateTime() != null) {
      trip.setDateTime(parseStringToDate(request.getDateTime()));
    }

    tripRepository.save(trip);
  }

  private void saveCoordinates(List<GenericPoint> coordinates, Trip trip) {
    int index = 0;
    for (GenericPoint genericPoint : coordinates) {
      Coordinate coordinate = new Coordinate();
      coordinate.setLatitude(Double.parseDouble(genericPoint.getLatitude()));
      coordinate.setLongitude(Double.parseDouble(genericPoint.getLongitude()));
      coordinate.setVehicle(trip.getVehicle());
      coordinate.setTrip(trip);
      coordinate.setPosition(index++);
      coordinateRepository.save(coordinate);
    }
  }

  private void deleteOldCoordinates(List<Coordinate> coordinates) {
    coordinateRepository.delete(coordinates);
  }

  private List<Trip> getAllTripsBetweenDates(TripSearchRequest request, Site site) {

    Date startDate = parseStringToDate(request.getInitialDateTime());
    Date endDate = parseStringToDate(request.getFinalDateTime());

    return tripRepository.findByDateTimeBetweenAndSite(startDate, endDate, site);
  }

  private List<Trip> getAllCloseTrips(TripSearchRequest request, Site site) {

    List<Trip> trips = getAllTripsBetweenDates(request, site);

    Coordinate userPosition = new Coordinate();

    double latitude = Double.parseDouble(request.getInitialPosition().getLatitude());
    double longitude = Double.parseDouble(request.getInitialPosition().getLongitude());

    userPosition.setLatitude(latitude);
    userPosition.setLongitude(longitude);

    List<Trip> tripsResponse = new ArrayList<>();

    for (Trip trip : trips) {
      int minorDistace = coordinateService.getMinorDistance(trip.getCoordinates(), userPosition);
      if (minorDistace <= request.getMaximumWalkingDistance()) {
        tripsResponse.add(trip);
      }
    }

    return tripsResponse;
  }
}
