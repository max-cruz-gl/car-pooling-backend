package com.globantlabs.carpooling.service.coordinate;

import com.globantlabs.carpooling.model.Coordinate;

import java.util.List;

public interface CoordinateService {
  int getMinorDistance(List<Coordinate> coordinates, Coordinate userPosition);
}
