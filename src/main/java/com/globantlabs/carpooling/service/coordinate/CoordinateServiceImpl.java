package com.globantlabs.carpooling.service.coordinate;

import com.globantlabs.carpooling.model.Coordinate;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CoordinateServiceImpl implements CoordinateService {

  @Override
  public int getMinorDistance(List<Coordinate> coordinates, Coordinate userPosition) {
    List<Integer> distances = new ArrayList<>();
    int index = 0;
    int coordinateNumbers = coordinates.size();

    while ((index + 1) < coordinateNumbers) {
      int distanceFromUser = distanceFromLine(coordinates.get(index), coordinates.get(index + 1), userPosition);
      distances.add(distanceFromUser);
      index++;
    }
    return getMinor(distances);
  }

  private int distanceFromLine(Coordinate initialPosition, Coordinate finalPosition, Coordinate userPosition) {

    double trianguleDistance = getTriangleDistance(initialPosition, finalPosition, userPosition);

    if ((trianguleDistance >= 0) && (trianguleDistance <= 1)) {
      return getMinorDistance(initialPosition, finalPosition, userPosition);
    }

    return getMinor(distance(initialPosition, userPosition), distance(finalPosition, userPosition));
  }

  private double getTriangleDistance(Coordinate initialPosition, Coordinate finalPosition, Coordinate userPosition) {
    double initialPositionLatitude = initialPosition.getLatitude();
    double initialPositionLongitude = initialPosition.getLongitude();
    double finalPositionLatitude = finalPosition.getLatitude();
    double finalPositionLongitude = finalPosition.getLongitude();
    double userPositionLatitude = userPosition.getLatitude();
    double userPositionLongitude = userPosition.getLongitude();

    double numerator = (userPositionLatitude - initialPositionLatitude) * (finalPositionLatitude - initialPositionLatitude)
        + (userPositionLongitude - initialPositionLongitude) * (finalPositionLongitude - initialPositionLongitude);

    double denomenator = (finalPositionLatitude - initialPositionLatitude) * (finalPositionLatitude - initialPositionLatitude)
        + (finalPositionLongitude - initialPositionLongitude) * (finalPositionLongitude - initialPositionLongitude);

    return numerator / denomenator;
  }

  private int getMinorDistance(Coordinate point1, Coordinate point2, Coordinate pointP) {

    GeodesicData geodesicData1toP = Geodesic.WGS84.Inverse(
        point1.getLatitude(), point1.getLongitude(),
        pointP.getLatitude(), pointP.getLongitude());

    GeodesicData geodesicData1to2 = Geodesic.WGS84.Inverse(
        point1.getLatitude(), point1.getLongitude(),
        point2.getLatitude(), point2.getLongitude());

    double distance1toP = geodesicData1toP.s12;

    double bearing1P = geodesicData1toP.azi1;
    double bearing12 = geodesicData1to2.azi1;

    double alfa = Math.toRadians(bearing1P - bearing12);

    return (int) Math.abs(Math.sin(alfa) * distance1toP);
  }

  private double distance(Coordinate A, Coordinate B) {
    GeodesicData geodesicDataAtoB = Geodesic.WGS84.Inverse(
        A.getLatitude(), A.getLongitude(),
        B.getLatitude(), B.getLongitude());
    return geodesicDataAtoB.s12;
  }

  private int getMinor(List<Integer> list) {
    return Collections.min(list);
  }

  private int getMinor(double positionOne, double positionTwo) {
    return positionOne < positionTwo ? (int) positionOne : (int) positionTwo;
  }
}
