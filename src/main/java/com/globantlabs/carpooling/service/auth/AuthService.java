package com.globantlabs.carpooling.service.auth;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

public interface AuthService {

  OAuth2AccessToken login(String email, String googleToken);
}
