package com.globantlabs.carpooling.service.auth;

import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.service.user.credentials.login.LoginService;
import com.globantlabs.carpooling.util.statics.OAuthStatics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AuthServiceImpl implements AuthService {

  public static final String CODE = "code";

  @Autowired
  private LoginService loginService;

  @Autowired
  protected AuthorizationServerTokenServices tokenServices;

  @Autowired
  protected TokenEndpoint tokenEndpoint;

  @Autowired
  protected TokenStore tokenStore;

  @Override
  public OAuth2AccessToken login(String email, String googleToken) {

    HashMap<String, String> parameters = new HashMap<>();

    parameters.put(OAuthStatics.GRANT_TYPE, OAuthStatics.PASSWORD);
    parameters.put(OAuthStatics.PASSWORD, googleToken);
    parameters.put(OAuthStatics.USERNAME, email);

    try {
      return tokenEndpoint(parameters);
    } catch (Exception exception) {
      throw new UserInvalidException();
    }
  }

  private OAuth2AccessToken tokenEndpoint(HashMap<String, String> parameters)
      throws HttpRequestMethodNotSupportedException {
    Set<String> responseTypes = new HashSet<>();
    responseTypes.add(CODE);

    List<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(new SimpleGrantedAuthority(OAuthStatics.ROLE_USER));

    HashSet<String> resourceIds = new HashSet<>();
    resourceIds.add(OAuthStatics.RESOURCE_ID);

    HashSet<String> scope = new HashSet<>();
    scope.add(OAuthStatics.SCOPE__OPENID);

    HashMap<String, Serializable> extensionProperties = new HashMap<>();
    HashMap<String, String> requestParameters = new HashMap<>();

    boolean approved = true;
    String redirectUri = null;

    OAuth2Request oauth2Request = new OAuth2Request(requestParameters, OAuthStatics.CP_CLIENT,
        authorities, approved, scope, resourceIds, redirectUri, responseTypes, extensionProperties);

    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
        OAuthStatics.CP_CLIENT, OAuthStatics.CP_SECRET, authorities);

    OAuth2Authentication auth = new OAuth2Authentication(oauth2Request, authenticationToken);

    return tokenEndpoint.postAccessToken(auth, parameters).getBody();
  }
}
