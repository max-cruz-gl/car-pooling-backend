package com.globantlabs.carpooling.service.site;

import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SiteServiceImpl implements SiteService {

  @Autowired
  private SiteRepository siteRepository;

  @Override
  public Site getSite(long id) {
    return siteRepository.findOne(id);
  }

  @Override
  public Iterable<Site> getAll() {
    return siteRepository.findAll();
  }

  @Override
  public Iterable<Site> getAll(String country) {
    return siteRepository.findByCountryIgnoringCase(country);
  }

  @Override
  public List<Trip> getAlltrips(long id) {
    Site site = siteRepository.findOne(id);
    return site.getTrips();
  }
}
