package com.globantlabs.carpooling.service.site;

import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.model.Trip;

import java.util.List;

public interface SiteService {

  Site getSite(long id);

  Iterable<Site> getAll();

  Iterable<Site> getAll(String country);

  List<Trip> getAlltrips(long id);
}
