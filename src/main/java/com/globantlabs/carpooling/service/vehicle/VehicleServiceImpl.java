package com.globantlabs.carpooling.service.vehicle;

import com.globantlabs.carpooling.controller.request.vehicle.VehicleRegistrationRequest;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VechicleInvalidException;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VehicleNotFoundException;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.model.Vehicle;
import com.globantlabs.carpooling.repository.VehicleRepository;
import com.globantlabs.carpooling.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class VehicleServiceImpl implements VehicleService {

  @Autowired
  private VehicleRepository vehicleRepository;

  @Autowired
  private UserService userService;


  @Override
  public void register(UserAccount userAccount, VehicleRegistrationRequest request) {
    Vehicle vehicle = new Vehicle();
    vehicle.setBrand(request.getBrand());
    vehicle.setColor(request.getColor());
    vehicle.setModel(request.getModel());
    vehicle.setType(request.getType());
    vehicle.setUserAccount(userAccount);
    vehicleRepository.save(vehicle);
  }

  @Override
  public void update(UserAccount userAccount,String id, VehicleRegistrationRequest request) {

    Vehicle vehicle = findVehicle(userAccount, id);

    if (!isBlank(request.getBrand())) {
      vehicle.setBrand(request.getBrand());
    }

    if (!isBlank(request.getColor())) {
      vehicle.setColor(request.getColor());
    }

    if (!isBlank(request.getModel())) {
      vehicle.setModel(request.getModel());
    }

    if (!isBlank(request.getType())) {
      vehicle.setType(request.getType());
    }

    vehicleRepository.save(vehicle);
  }

  @Override
  public void delete(UserAccount userAccount, String id) {
    Vehicle vehicle = findVehicle(userAccount, id);
    vehicleRepository.delete(vehicle);
  }

  private Vehicle findVehicle(UserAccount userAccount, String id) {
    Vehicle vehicle = vehicleRepository.findOne(Long.valueOf(id));

    if (vehicle == null) {
      throw new VehicleNotFoundException();
    }

    if (vehicle.getUserAccount().getId() != userAccount.getId()) {
      throw new VechicleInvalidException();
    }
    
    return vehicle;
  }

  @Override
  public Vehicle getVehicle(UserAccount userAccount, String id) {
    return vehicleRepository.findByIdAndUserAccount(Long.parseLong(id), userAccount);
  }
}
