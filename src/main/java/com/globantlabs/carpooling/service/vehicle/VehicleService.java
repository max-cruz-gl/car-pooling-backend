package com.globantlabs.carpooling.service.vehicle;

import com.globantlabs.carpooling.controller.request.vehicle.VehicleRegistrationRequest;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.model.Vehicle;

public interface VehicleService {

  void register(UserAccount userAccount, VehicleRegistrationRequest request);

  void update(UserAccount userAccount, String id, VehicleRegistrationRequest request);

  void delete(UserAccount userAccount, String id);

  Vehicle getVehicle(UserAccount userAccount, String id);
}
