package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.exception.generic.BadRequestException;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.validation.BindingResult;

public abstract class CarPoolingController {

  @Autowired
  protected UserRepository userRepository;

  public void verifyValidationErrors(BindingResult result) {
    if (result.hasErrors()) {
      throw new BadRequestException(result);
    }
  }

  protected String getTokenValue() {
    try {
      return ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication()
          .getDetails()).getTokenValue();
    } catch (Exception e) {
      throw new InvalidTokenException("");
    }
  }

  public UserAccount verifyAuthorizationToken() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();


    if (auth == null) {
      throw new UserInvalidException();
    }

    String email = auth.getName();

    if (email == null) {
      throw new UserInvalidException();
    }

    UserAccount user = userRepository.findByEmail(email);

    if (user == null) {
      throw new UserInvalidException();
    }

    user.updateLastAction();
    userRepository.save(user);

    return user;
  }
}
