package com.globantlabs.carpooling.controller.responses.vehicle;

import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.model.Vehicle;

import java.util.List;

public class VehicleResponse extends OkResponse{

  private List<Vehicle> vehicles;

  public List<Vehicle> getVehicles() {
    return vehicles;
  }

  public void setVehicles(List<Vehicle> vehicles) {
    this.vehicles = vehicles;
  }
}
