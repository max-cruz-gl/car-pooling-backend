package com.globantlabs.carpooling.controller.responses;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ErrorResponse extends OkResponse {

  protected Map<String, String> errors;

  public ErrorResponse() {
    this.status = "Error";
    this.errors = new TreeMap<>();
  }

  public Map<String, String> getErrors() {
    return errors;
  }

  public void setErrors(Map<String, String> errors) {
    this.errors = errors;
  }

  /**
   * .
   */
  public void addError(String code, String message) {
    if (!errors.containsKey(code)) {
      errors.put(code, message);
    }
  }

  protected void addFieldErrors(List<FieldError> result) {
    for (FieldError error : result) {
      errors.put(error.getDefaultMessage(), error.getField());
    }
  }

  protected void addObjectErrors(List<ObjectError> result) {
    for (ObjectError error : result) {
      if (!errors.containsKey(error.getDefaultMessage())) {
        errors.put(error.getDefaultMessage(), error.getCode());
      }
    }
  }

  /**
   * Method to add errors found by Spring validators..
   */
  public void addErrors(BindingResult result) {
    addFieldErrors(result.getFieldErrors());
    addObjectErrors(result.getAllErrors());
  }

  public boolean hasErrors() {
    return errors.size() != 0;
  }
}
