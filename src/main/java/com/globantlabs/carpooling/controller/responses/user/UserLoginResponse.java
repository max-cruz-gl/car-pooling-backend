package com.globantlabs.carpooling.controller.responses.user;

import com.globantlabs.carpooling.controller.responses.OkResponse;

public class UserLoginResponse extends OkResponse {

  private String accessToken;
  private String refreshToken;

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }
}
