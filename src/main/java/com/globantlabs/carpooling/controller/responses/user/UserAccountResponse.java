package com.globantlabs.carpooling.controller.responses.user;

import com.globantlabs.carpooling.controller.responses.OkResponse;

import java.util.Date;

public class UserAccountResponse extends OkResponse {

  private String name;
  private String lastName;
  private String address;
  private String email;
  private String pictureUrl;
  private boolean driver;
  private Date lastAction;
  private String userStatus;
  private long site;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPictureUrl() {
    return pictureUrl;
  }

  public void setPictureUrl(String pictureUrl) {
    this.pictureUrl = pictureUrl;
  }

  public boolean isDriver() {
    return driver;
  }

  public void setDriver(boolean driver) {
    this.driver = driver;
  }

  public Date getLastAction() {
    return lastAction;
  }

  public void setLastAction(Date lastAction) {
    this.lastAction = lastAction;
  }

  public String getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(String userStatus) {
    this.userStatus = userStatus;
  }

  public long getSite() {
    return site;
  }

  public void setSite(long site) {
    this.site = site;
  }
}
