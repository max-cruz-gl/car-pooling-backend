package com.globantlabs.carpooling.controller.responses;

public class OkResponse {

  protected String status;

  public OkResponse() {
    this.status = "OK";
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}
