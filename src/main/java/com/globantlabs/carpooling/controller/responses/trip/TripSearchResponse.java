package com.globantlabs.carpooling.controller.responses.trip;

import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.model.Trip;

import java.util.List;

public class TripSearchResponse extends OkResponse {

  private List<Trip> trips;

  public List<Trip> getTrips() {
    return trips;
  }

  public void setTrips(List<Trip> trips) {
    this.trips = trips;
  }
}
