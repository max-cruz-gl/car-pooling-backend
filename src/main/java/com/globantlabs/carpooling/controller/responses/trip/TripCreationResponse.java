package com.globantlabs.carpooling.controller.responses.trip;

import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.model.Point;

import java.util.List;

public class TripCreationResponse extends OkResponse {

  private List<Point> points;

  public List<Point> getPoints() {
    return points;
  }

  public void setPoints(List<Point> points) {
    this.points = points;
  }
}
