package com.globantlabs.carpooling.controller.responses.trip;

import com.globantlabs.carpooling.model.Coordinate;
import com.globantlabs.carpooling.model.Vehicle;

import java.util.List;

public class TripInformationResponse {

  private String dateTime;
  private int availableSeats;
  private int pendingConfirmedSeats;
  private long driverId;
  private Vehicle vehicle;
  private List<Coordinate> coordinates;

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public int getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(int availableSeats) {
    this.availableSeats = availableSeats;
  }

  public int getPendingConfirmedSeats() {
    return pendingConfirmedSeats;
  }

  public void setPendingConfirmedSeats(int pendingConfirmedSeats) {
    this.pendingConfirmedSeats = pendingConfirmedSeats;
  }

  public long getDriverId() {
    return driverId;
  }

  public void setDriverId(long driverId) {
    this.driverId = driverId;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public List<Coordinate> getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(List<Coordinate> coordinates) {
    this.coordinates = coordinates;
  }
}
