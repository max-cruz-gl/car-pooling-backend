package com.globantlabs.carpooling.controller.responses.notification;

import com.globantlabs.carpooling.model.GenericNotification;

import java.util.List;

public class NotificationsResponse {

  List<GenericNotification> notifications;

  public List<GenericNotification> getNotifications() {
    return notifications;
  }

  public void setNotifications(List<GenericNotification> notifications) {
    this.notifications = notifications;
  }
}
