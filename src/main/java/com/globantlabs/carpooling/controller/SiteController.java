package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.service.site.SiteService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SiteController  extends CarPoolingController {

  @Autowired
  private SiteService siteService;

  @ResponseBody
  @RequestMapping(value = "/site", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Get all sites", tags = "site")
  public ResponseEntity<Iterable<Site>> getSites() {
    return ResponseEntity.ok(siteService.getAll());
  }

  @ResponseBody
  @RequestMapping(value = "/site/{country}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Get all sites", tags = "site")
  public ResponseEntity<Iterable<Site>> getSites(@PathVariable("country") String country) {
    return ResponseEntity.ok(siteService.getAll(country));
  }
}
