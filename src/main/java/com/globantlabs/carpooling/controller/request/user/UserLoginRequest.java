package com.globantlabs.carpooling.controller.request.user;

public class UserLoginRequest {

  private String googleToken;
  private String email;

  public String getGoogleToken() {
    return googleToken;
  }

  public void setGoogleToken(String googleToken) {
    this.googleToken = googleToken;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
