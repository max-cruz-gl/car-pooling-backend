package com.globantlabs.carpooling.controller.request.trip;

import com.globantlabs.carpooling.controller.responses.OkResponse;

public class TripSelectResponse extends OkResponse{

  private boolean confirm;

  public boolean isConfirm() {
    return confirm;
  }

  public void setConfirm(boolean confirm) {
    this.confirm = confirm;
  }
}
