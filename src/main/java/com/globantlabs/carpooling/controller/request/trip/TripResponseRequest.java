package com.globantlabs.carpooling.controller.request.trip;

import javax.validation.constraints.NotNull;

public class TripResponseRequest {

  @NotNull
  private long passengerId;

  @NotNull
  private long tripId;

  @NotNull
  private String status;

  private String description;

  public long getPassengerId() {
    return passengerId;
  }

  public void setPassengerId(long passengerId) {
    this.passengerId = passengerId;
  }

  public long getTripId() {
    return tripId;
  }

  public void setTripId(long tripId) {
    this.tripId = tripId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
