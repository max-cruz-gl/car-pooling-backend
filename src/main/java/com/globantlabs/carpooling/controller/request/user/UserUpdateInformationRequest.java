package com.globantlabs.carpooling.controller.request.user;

import com.globantlabs.carpooling.util.validators.site.ValidSite;

public class UserUpdateInformationRequest {

  private String address;
  private String driver;

  @ValidSite
  private long site;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }

  public long getSite() {
    return site;
  }

  public void setSite(long site) {
    this.site = site;
  }
}
