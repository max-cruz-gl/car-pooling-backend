package com.globantlabs.carpooling.controller.request.trip;

import com.globantlabs.carpooling.model.GenericPoint;
import com.globantlabs.carpooling.util.validators.site.ValidSite;

import java.util.List;

public class TripUpdateRequest {

  private Long vehicleId;

  private List<GenericPoint> coordinates;

  @ValidSite
  private Long siteId;

  private Integer availableSeats;

  private String dateTime;

  public Long getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(Long vehicleId) {
    this.vehicleId = vehicleId;
  }

  public List<GenericPoint> getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(List<GenericPoint> coordinates) {
    this.coordinates = coordinates;
  }

  public Long getSiteId() {
    return siteId;
  }

  public void setSiteId(Long siteId) {
    this.siteId = siteId;
  }

  public Integer getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(Integer availableSeats) {
    this.availableSeats = availableSeats;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }
}
