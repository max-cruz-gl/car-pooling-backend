package com.globantlabs.carpooling.controller.request.trip;

import com.globantlabs.carpooling.model.GenericPoint;
import com.globantlabs.carpooling.util.validators.site.ValidSite;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TripCreationRequest {

  @NotNull
  private long vehicleId;

  @NotNull
  private List<GenericPoint> coordinates;

  @ValidSite
  private long siteId;

  @NotNull
  private int availableSeats;

  @NotNull
  private String dateTime;

  public long getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(long vehicleId) {
    this.vehicleId = vehicleId;
  }

  public List<GenericPoint> getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(List<GenericPoint> coordinates) {
    this.coordinates = coordinates;
  }

  public long getSiteId() {
    return siteId;
  }

  public void setSiteId(long siteId) {
    this.siteId = siteId;
  }

  public int getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(int availableSeats) {
    this.availableSeats = availableSeats;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }
}
