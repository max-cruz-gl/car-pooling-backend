package com.globantlabs.carpooling.controller.request.trip;

import com.globantlabs.carpooling.model.GenericPoint;
import com.globantlabs.carpooling.util.validators.site.ValidSite;

import javax.validation.constraints.NotNull;

public class TripSearchRequest {

  private GenericPoint initialPosition;

  @ValidSite
  private long finalSiteId;

  @NotNull
  private String initialDateTime;

  @NotNull
  private String finalDateTime;

  private Integer maximumWalkingDistance;

  public GenericPoint getInitialPosition() {
    return initialPosition;
  }

  public void setInitialPosition(GenericPoint initialPosition) {
    this.initialPosition = initialPosition;
  }

  public long getFinalSiteId() {
    return finalSiteId;
  }

  public void setFinalSiteId(long finalSiteId) {
    this.finalSiteId = finalSiteId;
  }

  public String getInitialDateTime() {
    return initialDateTime;
  }

  public void setInitialDateTime(String initialDateTime) {
    this.initialDateTime = initialDateTime;
  }

  public String getFinalDateTime() {
    return finalDateTime;
  }

  public void setFinalDateTime(String finalDateTime) {
    this.finalDateTime = finalDateTime;
  }

  public Integer getMaximumWalkingDistance() {
    return maximumWalkingDistance;
  }

  public void setMaximumWalkingDistance(Integer maximumWalkingDistance) {
    this.maximumWalkingDistance = maximumWalkingDistance;
  }
}
