package com.globantlabs.carpooling.controller.request.user;

import com.globantlabs.carpooling.util.statics.UserStatics;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class UserRegistrationRequest {

  @NotNull
  @Size(min = 1, max = 50)
  @Pattern(regexp = UserStatics.EMAIL_PATTERN)
  private String email;

  @NotNull
  @Size(min = 1, max = 20)
  private String googleToken;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getGoogleToken() {
    return googleToken;
  }

  public void setGoogleToken(String googleToken) {
    this.googleToken = googleToken;
  }
}
