package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.controller.request.trip.TripCreationRequest;
import com.globantlabs.carpooling.controller.request.trip.TripResponseRequest;
import com.globantlabs.carpooling.controller.request.trip.TripSearchRequest;
import com.globantlabs.carpooling.controller.request.trip.TripUpdateRequest;
import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.controller.responses.trip.TripInformationResponse;
import com.globantlabs.carpooling.controller.responses.trip.TripSearchResponse;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.service.trip.TripService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TripController extends CarPoolingController {

  @Autowired
  private TripService tripService;

  @ResponseBody
  @RequestMapping(value = "/trip", method = RequestMethod.POST)
  @ApiOperation(value = "Create a trip", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> postTrip(@RequestBody @Validated TripCreationRequest tripCreationRequest) {
    UserAccount userAccount = verifyAuthorizationToken();
    tripService.create(userAccount, tripCreationRequest);

    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/trip", method = RequestMethod.GET)
  @ApiOperation(value = "Get all trip from user", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<List<Trip>> getAllTrips() {
    UserAccount userAccount = verifyAuthorizationToken();
    return ResponseEntity.ok(userAccount.getTrips());
  }

  @ResponseBody
  @RequestMapping(value = "/trip/{id}", method = RequestMethod.GET)
  @ApiOperation(value = "Trip information", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<TripInformationResponse> getTrip(@PathVariable("id") long id) {
    verifyAuthorizationToken();
    return ResponseEntity.ok(tripService.getTrip(id));
  }

  @ResponseBody
  @RequestMapping(value = "/trip/{id}", method = RequestMethod.DELETE)
  @ApiOperation(value = "Delete trip", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> deleteTrip(@PathVariable("id") long id) {
    UserAccount userAccount = verifyAuthorizationToken();
    tripService.deleteTrip(id, userAccount);
    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/trip/{id}", method = RequestMethod.PUT)
  @ApiOperation(value = "Update a trip", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> updateTrip(@PathVariable("id") long id,
                                               @RequestBody @Validated TripUpdateRequest request) {
    UserAccount userAccount = verifyAuthorizationToken();
    tripService.updateTrip(id, userAccount, request);
    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/trip/search", method = RequestMethod.POST)
  @ApiOperation(value = "Create a trip", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<TripSearchResponse> getTrips(@RequestBody @Validated TripSearchRequest tripSearchRequest) {
    verifyAuthorizationToken();
    return ResponseEntity.ok(tripService.search(tripSearchRequest));
  }

  @ResponseBody
  @RequestMapping(value = "/trip/request/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "User request a trip", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> requestTrip(@PathVariable("id") long id) {
    UserAccount userAccount = verifyAuthorizationToken();
    tripService.request(id, userAccount);

    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/trip/response", method = RequestMethod.POST)
  @ApiOperation(value = "Driver response to a trip request", tags = "trip")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> confirmTrip(@RequestBody @Validated TripResponseRequest request) {
    verifyAuthorizationToken();
    tripService.response(request);
    return ResponseEntity.ok(new OkResponse());
  }
}
