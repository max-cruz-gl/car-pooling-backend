package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.controller.request.user.UserLoginRequest;
import com.globantlabs.carpooling.controller.request.user.UserUpdateInformationRequest;
import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.controller.responses.user.UserAccountResponse;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.service.user.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController extends CarPoolingController {

  @Autowired
  private UserService userService;

  @ResponseBody
  @RequestMapping(value = "/user/profile", method = RequestMethod.GET)
  @ApiOperation(value = "User information", tags = "user")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<UserAccountResponse> getUserInformation() {
    UserAccount userAccount = verifyAuthorizationToken();
    return ResponseEntity.ok(userService.getUserInformation(userAccount));
  }

  @ResponseBody
  @RequestMapping(value = "/user", method = RequestMethod.PUT)
  @ApiOperation(value = "Update user information", tags = "user")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> postUserInformation(@RequestBody @Validated UserUpdateInformationRequest userUpdateInformationRequest) {
    UserAccount userAccount = verifyAuthorizationToken();
    userService.updateUserInformation(userAccount, userUpdateInformationRequest);
    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/user/login", method = RequestMethod.POST)
  @ApiOperation(value = "Register a user", tags = "user")
  public ResponseEntity<OkResponse> postRegistration(@RequestBody @Validated UserLoginRequest userLoginRequest) {
    return ResponseEntity.ok(userService.loginUser(userLoginRequest));
  }
}
