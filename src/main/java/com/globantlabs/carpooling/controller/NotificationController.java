package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.controller.responses.notification.NotificationsResponse;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.service.notification.NotificationService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController extends CarPoolingController  {

  @Autowired
  private NotificationService notificationService;

  @ResponseBody
  @RequestMapping(value = "/notification/received", method = RequestMethod.GET)
  @ApiOperation(value = "Get all received user notification", tags = "notification")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<NotificationsResponse> getReceivedNotifications() {
    UserAccount userAccount = verifyAuthorizationToken();

    return ResponseEntity.ok(notificationService.getReceivedNotifications(userAccount));
  }

  @ResponseBody
  @RequestMapping(value = "/notification/sent", method = RequestMethod.GET)
  @ApiOperation(value = "Get all notifications sent by the user", tags = "notification")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<NotificationsResponse> getSendNotifications() {
    UserAccount userAccount = verifyAuthorizationToken();

    return ResponseEntity.ok(notificationService.getSendNotifications(userAccount));
  }
}
