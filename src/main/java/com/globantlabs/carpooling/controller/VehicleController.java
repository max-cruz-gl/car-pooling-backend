package com.globantlabs.carpooling.controller;

import com.globantlabs.carpooling.controller.request.vehicle.VehicleRegistrationRequest;
import com.globantlabs.carpooling.controller.responses.OkResponse;
import com.globantlabs.carpooling.controller.responses.vehicle.VehicleResponse;
import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.model.Vehicle;
import com.globantlabs.carpooling.service.vehicle.VehicleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VehicleController extends CarPoolingController {

  @Autowired
  private VehicleService vehicleService;

  @ResponseBody
  @RequestMapping(value = "/vehicle", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Vehicles information", tags = "vehicle")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<VehicleResponse> getVehicleInformation() {
    UserAccount userAccount = verifyAuthorizationToken();

    VehicleResponse vehicleResponse = new VehicleResponse();
    vehicleResponse.setVehicles(userAccount.getVehicles());

    return ResponseEntity.ok(vehicleResponse);
  }

  @ResponseBody
  @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Vehicle information", tags = "vehicle")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<Vehicle> getVehicleInformation(@PathVariable("id") String id) {
    UserAccount userAccount = verifyAuthorizationToken();
    return ResponseEntity.ok(vehicleService.getVehicle(userAccount, id));
  }

  @ResponseBody
  @RequestMapping(value = "/vehicle", method = RequestMethod.POST)
  @ApiOperation(value = "Add vehicles to user", tags = "vehicle")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> postVehicle(@RequestBody @Validated VehicleRegistrationRequest request) {
    UserAccount userAccount = verifyAuthorizationToken();

    vehicleService.register(userAccount, request);

    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.PUT)
  @ApiOperation(value = "Update user's vehicle", tags = "vehicle")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> updateVehicle(@PathVariable("id") String id,@RequestBody @Validated VehicleRegistrationRequest request) {
    UserAccount userAccount = verifyAuthorizationToken();

    vehicleService.update(userAccount, id, request);

    return ResponseEntity.ok(new OkResponse());
  }

  @ResponseBody
  @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.DELETE)
  @ApiOperation(value = "Update user's vehicle", tags = "vehicle")
  @ApiImplicitParam(name = "Authorization", value = "Bearer token", dataType = "string", paramType = "header")
  public ResponseEntity<OkResponse> deleteVehicle(@PathVariable("id") String id) {
    UserAccount userAccount = verifyAuthorizationToken();

    vehicleService.delete(userAccount, id);

    return ResponseEntity.ok(new OkResponse());
  }
}
