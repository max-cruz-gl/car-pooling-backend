package com.globantlabs.carpooling.facade;

import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.model.UserAccount;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;

@Component
public class GoogleUserInformationApiFacade {

  private static final String CLIENT_SECRET_FILE = "client_secret.json";
  private static final String REDIRECT_URI = "";
  private static final String TOKEN_SERVER_ENCODED_URI = "https://www.googleapis.com/oauth2/v4/token";

  public UserAccount getUserInformation(UserAccount userAccount) throws IOException {
    GoogleIdToken.Payload payload = getGoogleInformation(userAccount.getGoogleToken());
    return parserPayloadGoogleIdTokenToUserAccount(userAccount, payload);
  }

  private GoogleIdToken.Payload getGoogleInformation(String authCode) throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();

    GoogleClientSecrets clientSecrets =
        GoogleClientSecrets.load(
            JacksonFactory.getDefaultInstance(),
            new FileReader(classLoader.getResource(CLIENT_SECRET_FILE).getFile()));

    GoogleTokenResponse tokenResponse =
        new GoogleAuthorizationCodeTokenRequest(
            new NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            TOKEN_SERVER_ENCODED_URI,
            clientSecrets.getDetails().getClientId(),
            clientSecrets.getDetails().getClientSecret(),
            authCode,
            REDIRECT_URI).execute();

    GoogleIdToken idToken = tokenResponse.parseIdToken();

    return idToken.getPayload();
  }

  private UserAccount parserPayloadGoogleIdTokenToUserAccount(UserAccount userAccount,
                                                              GoogleIdToken.Payload payload) {
    userAccount.setGoogleToken(userAccount.getGoogleToken());
    userAccount.setName((String) payload.get("given_name"));
    userAccount.setPictureUrl((String) payload.get("picture"));
    userAccount.setLastName((String) payload.get("family_name"));
    if(!payload.getEmail().equals(userAccount.getEmail())) {
      throw new UserInvalidException();
    }
    return userAccount;
  }
}
