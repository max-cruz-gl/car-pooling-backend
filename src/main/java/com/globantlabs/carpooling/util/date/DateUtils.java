package com.globantlabs.carpooling.util.date;

import com.globantlabs.carpooling.exception.generic.GenericCarPoolingException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
  private static final String FORMAT_DATE = "dd/MM/yyyy HH:mm";
  private static final String INVALID_DATE_MESSAGE = "Invalid date";

  public static Date parseStringToDate(String date) {
    try {
      SimpleDateFormat df = new SimpleDateFormat(FORMAT_DATE);
      return df.parse(date);
    } catch (ParseException e) {
      throw new GenericCarPoolingException(INVALID_DATE_MESSAGE);
    }
  }
}
