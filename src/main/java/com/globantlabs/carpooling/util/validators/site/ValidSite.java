package com.globantlabs.carpooling.util.validators.site;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = SiteValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidSite {

  /**
   * .
   *
   * @return .
   */
  String message() default "{Site}";

  /**
   * .
   *
   * @return .
   */
  Class<?>[] groups() default {};

  /**
   * .
   *
   * @return .
   */
  Class<? extends Payload>[] payload() default {};

}