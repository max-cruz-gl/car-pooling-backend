package com.globantlabs.carpooling.util.validators.site;

import com.globantlabs.carpooling.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SiteValidator implements ConstraintValidator<ValidSite, Long> {

  @Autowired
  protected SiteRepository siteRepository;

  protected ValidSite annotation;

  @Override
  public void initialize(ValidSite annotation) {
    this.annotation = annotation;
  }

  @Override
  public boolean isValid(Long field, ConstraintValidatorContext cxt) {
    return field == null || siteRepository.exists(field);
  }
}
