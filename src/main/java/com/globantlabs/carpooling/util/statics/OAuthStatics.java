package com.globantlabs.carpooling.util.statics;

public class OAuthStatics {
  public static final String GRANT_TYPE = "grant_type";
  public static final String PASSWORD = "password";
  public static final String USERNAME = "username";

  public static final String CP_CLIENT = "carpooling";
  public static final String CP_SECRET = "cpsecret";


  public static final String ROLE_USER = "ROLE_USER";

  public static final String RESOURCE_ID = "CARPOOLING_REST_API";
  public static final String SCOPE__OPENID = "openid";

  public static final int ACCESS_TOKEN_EXPIRATION = 200 * 60;
  public static final int REFRESH_TOKEN_EXPIRATION = 10 * ACCESS_TOKEN_EXPIRATION;

  public static final String SESSION__ACTIVE = "active";
  public static final String SESSION__OVERWRITTEN = "overwritten";
  public static final String SESSION__EXPIRED = "expired";
}
