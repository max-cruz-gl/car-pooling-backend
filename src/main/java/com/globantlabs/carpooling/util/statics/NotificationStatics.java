package com.globantlabs.carpooling.util.statics;

public class NotificationStatics {
  public static final String STATUS_PENDING_APPROVAL = "pending";
  public static final String STATUS_ACCEPTED = "accepted";
  public static final String STATUS_REJECTED = "rejected";
  public static final String STATUS_DELETED = "deleted";

  public static final String PASSENGER_REQUEST_TRIP = "request";
  public static final String DRIVER_RESPONSE_TRIP = "response";

  public static final String PASSENGER_ACCEPTED_DESCRIPTION = "Enhorabuena, han aceptado su solicud para unirse a un viaje";
  public static final String PASSENGER_REJECTED_DESCRIPTION = "Desafortunadamente, han rechazado su solicud para unirse a un viaje";

  public static final String PASSENGER_REQUEST_TRIP_DESCRIPTION = "Han solicitado unirse a un viaje.";
  public static final String DRIVER_ACCEPTED_DESCRIPTION = "Has aceptado su solicud para unirse a un viaje.";
  public static final String DRIVER_REJECTED_DESCRIPTION = "Has rechazado su solicud para unirse a un viaje.";

}
