package com.globantlabs.carpooling.util.statics;

public class UserStatusStatics {
  public static final String STATUS_REGISTER_COMPLETED = "register";
  public static final String STATUS_ADDITIONAL_COMPLETED = "additional";
  public static final String STATUS_COMPLETED = "completed";
}
