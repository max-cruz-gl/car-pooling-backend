package com.globantlabs.carpooling.util.statics;

public class UserStatics {
  public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@globant.com$";
}
