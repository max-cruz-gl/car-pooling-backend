package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.Coordinate;
import org.springframework.data.repository.CrudRepository;

public interface CoordinateRepository extends CrudRepository<Coordinate, String> {

}
