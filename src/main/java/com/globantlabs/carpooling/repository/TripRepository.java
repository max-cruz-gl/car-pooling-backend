package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.Site;
import com.globantlabs.carpooling.model.Trip;
import com.globantlabs.carpooling.model.UserAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface TripRepository extends CrudRepository<Trip, Long> {
  List<Trip> findByDateTimeBetweenAndSite(Date start, Date end, Site site);
}
