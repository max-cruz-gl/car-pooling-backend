package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.Site;
import org.springframework.data.repository.CrudRepository;

public interface SiteRepository extends CrudRepository<Site,Long> {
  Iterable<Site> findByCountryIgnoringCase(String country);
}
