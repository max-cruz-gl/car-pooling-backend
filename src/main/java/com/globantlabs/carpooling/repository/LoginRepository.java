package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.Login;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends CrudRepository<Login, String> {
}
