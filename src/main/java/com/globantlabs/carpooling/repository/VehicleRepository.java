package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.UserAccount;
import com.globantlabs.carpooling.model.Vehicle;
import org.springframework.data.repository.CrudRepository;

public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
  Vehicle findByIdAndUserAccount(long id, UserAccount userAccount);
}