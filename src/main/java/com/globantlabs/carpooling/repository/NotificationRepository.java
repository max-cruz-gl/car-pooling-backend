package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.Notification;
import com.globantlabs.carpooling.model.Trip;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotificationRepository extends CrudRepository<Notification, Long> {

  Notification findByTripAndTransmitter(Trip trip, long transmitter);

  List<Notification> findByTransmitter(long id);
}
