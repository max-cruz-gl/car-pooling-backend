package com.globantlabs.carpooling.repository;

import com.globantlabs.carpooling.model.UserAccount;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserAccount,Long> {
  UserAccount findByEmail(String email);

  UserAccount findByGoogleToken(String googleToken);
}
