package com.globantlabs.carpooling.exception.google;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class GoogleUserInformationApiException extends CarPoolingException {
  public GoogleUserInformationApiException() {
    super("Something went wrong", "2000");
  }
}
