package com.globantlabs.carpooling.exception.generic;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class GenericCarPoolingException extends CarPoolingException {

  public GenericCarPoolingException(String error) {
    super(error, "0001");
  }
}
