package com.globantlabs.carpooling.exception.generic;

import org.springframework.validation.BindingResult;

public class BadRequestException extends RuntimeException {

  protected BindingResult result;

  public BadRequestException(BindingResult result) {
    this.result = result;
  }

  public BindingResult getResult() {
    return result;
  }

  public void setResult(BindingResult result) {
    this.result = result;
  }

}