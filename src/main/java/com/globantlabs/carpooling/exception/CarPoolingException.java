package com.globantlabs.carpooling.exception;

public class CarPoolingException extends RuntimeException {
  private String errorCode;

  public CarPoolingException() {
    super();
  }

  public CarPoolingException(String errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }

  public String getErrorCode() {
    return errorCode;
  }
}
