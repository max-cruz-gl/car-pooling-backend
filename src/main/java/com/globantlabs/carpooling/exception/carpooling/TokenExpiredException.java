package com.globantlabs.carpooling.exception.carpooling;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class TokenExpiredException extends CarPoolingException {

  private String message;

  public TokenExpiredException(String token) {
    super("Token expired " + token, "9998");
    this.message = "Token expired " + token;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public String getLocalizedMessage() {
    return message;
  }
}
