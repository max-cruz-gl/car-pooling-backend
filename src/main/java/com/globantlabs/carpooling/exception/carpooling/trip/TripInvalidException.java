package com.globantlabs.carpooling.exception.carpooling.trip;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class TripInvalidException extends CarPoolingException {

  public TripInvalidException() {
    super("Trip invalid", "5000");
  }
}
