package com.globantlabs.carpooling.exception.carpooling.trip;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class TripNotFoundException extends CarPoolingException {

  public TripNotFoundException() {
    super("Can not fint Trip", "5001");
  }
}