package com.globantlabs.carpooling.exception.carpooling;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class InvalidTokenException extends CarPoolingException {

  private String message;

  public InvalidTokenException(String token) {
    super("Invalid token: " + token, "9999");
    this.message = "Invalid token: " + token;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public String getLocalizedMessage() {
    return message;
  }
}
