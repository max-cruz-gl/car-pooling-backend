package com.globantlabs.carpooling.exception.carpooling.vehicle;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class VechicleInvalidException extends CarPoolingException {

  public VechicleInvalidException() {
    super("Invalid vehicle", "3002");
  }
}