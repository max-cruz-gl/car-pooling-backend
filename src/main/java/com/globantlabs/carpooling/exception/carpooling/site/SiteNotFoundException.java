package com.globantlabs.carpooling.exception.carpooling.site;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class SiteNotFoundException extends CarPoolingException {

  public SiteNotFoundException() {
    super("Can not find site", "4000");
  }
}
