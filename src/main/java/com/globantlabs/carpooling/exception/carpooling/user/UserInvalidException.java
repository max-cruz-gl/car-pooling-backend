package com.globantlabs.carpooling.exception.carpooling.user;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class UserInvalidException extends CarPoolingException {

  public UserInvalidException() {
    super("Invalid user", "1000");
  }

  public UserInvalidException(String string) {
    super(string, "1000");
  }
}
