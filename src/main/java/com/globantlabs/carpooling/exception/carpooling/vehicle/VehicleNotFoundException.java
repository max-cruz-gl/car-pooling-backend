package com.globantlabs.carpooling.exception.carpooling.vehicle;

import com.globantlabs.carpooling.exception.CarPoolingException;

public class VehicleNotFoundException extends CarPoolingException {

  public VehicleNotFoundException() {
    super("Can not found vehicle", "3001");
  }
}