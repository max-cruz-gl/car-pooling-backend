package com.globantlabs.error;

import com.globantlabs.carpooling.controller.responses.ErrorResponse;
import com.globantlabs.carpooling.exception.carpooling.InvalidTokenException;
import com.globantlabs.carpooling.exception.carpooling.TokenExpiredException;
import com.globantlabs.carpooling.exception.carpooling.site.SiteNotFoundException;
import com.globantlabs.carpooling.exception.carpooling.trip.TripInvalidException;
import com.globantlabs.carpooling.exception.carpooling.trip.TripNotFoundException;
import com.globantlabs.carpooling.exception.carpooling.user.UserInvalidException;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VechicleInvalidException;
import com.globantlabs.carpooling.exception.carpooling.vehicle.VehicleNotFoundException;
import com.globantlabs.carpooling.exception.generic.BadRequestException;
import com.globantlabs.carpooling.exception.generic.GenericCarPoolingException;
import com.globantlabs.carpooling.exception.google.GoogleUserInformationApiException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorHandler {

  /**
   * Handler for the exception that triggers when some error has occurred.
   */
  @ExceptionHandler(GenericCarPoolingException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse genericCarPoolingExceptionHandling(GenericCarPoolingException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when a user is invalid.
   */
  @ExceptionHandler(UserInvalidException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ResponseBody
  public ErrorResponse userinvalidExceptionHandling(UserInvalidException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when google server has error.
   */
  @ExceptionHandler(GoogleUserInformationApiException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public ErrorResponse googleUserApiExceptionHandling(GoogleUserInformationApiException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }


  /**
   * Handler for the exception that triggers when a vehicle is invalid.
   */
  @ExceptionHandler(VechicleInvalidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse invalidVehicleExceptionHandling(VechicleInvalidException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when can not fount a vehicle.
   */
  @ExceptionHandler(VehicleNotFoundException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse notFoundVehicleExceptionHandling(VehicleNotFoundException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when can not fount a site.
   */
  @ExceptionHandler(SiteNotFoundException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse notFoundSiteExceptionHandling(SiteNotFoundException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when a trip is invalid.
   */
  @ExceptionHandler(TripInvalidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse invalidTripExceptionHandling(TripInvalidException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when can not fount a trip.
   */
  @ExceptionHandler(TripNotFoundException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse notFoundSiteExceptionHandling(TripNotFoundException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when a token is invalid.
   */
  @ExceptionHandler(InvalidTokenException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse invalidTokenExceptionHandling(InvalidTokenException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when a token is expired.
   */
  @ExceptionHandler(TokenExpiredException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ResponseBody
  public ErrorResponse tokenExpiredExceptionHandling(TokenExpiredException exception) {
    ErrorResponse response = new ErrorResponse();
    response.setStatus("Error");
    response.addError(exception.getErrorCode(), exception.getMessage());

    return response;
  }

  /**
   * Handler for the exception that triggers when a user can't be found.
   */
  @ExceptionHandler(BadRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse nonBadRequestException(BadRequestException exception) {
    ErrorResponse response = new ErrorResponse();
    response.addErrors(exception.getResult());

    return response;
  }
}
